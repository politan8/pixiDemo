var path = require("path");
var webpack = require("webpack");
var CleanPlugin = require("clean-webpack-plugin");
var BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

module.exports = {
    entry: {
        app: "./src/resources/ts/com/DemoClient.ts",

        // vendor bundle, see aliases & CommonsChunkPlugin
        vendor: [
            "tslib",
            "core-js/es6",
            "core-js/stage/4",
            "gsap",
            "stats",
            "pixi",
            "pixi-particles",
            "node-emoji"
        ]
    },

    // define folder & filename of application bundle
    output: {
        path:     __dirname + "/dist/",
        filename: "js/app.bundle.js",
        library:  "DemoClient"
    },

    // specify the extensions used to resolve modules
    resolve: {
        extensions: [".ts", ".js"],

        alias: {
            "pixi":   path.join(__dirname, "/node_modules/pixi.js/dist/pixi.js"),
            "stats":   path.join(__dirname, "/src/resources/lib/Stats.js"),
            "pixi-particles":   path.join(__dirname, "/node_modules/pixi-particles/dist/pixi-particles.js"),
            "node-emoji":   path.join(__dirname, "/node_modules/node-emoji/lib/emoji.js")
        }
    },

    plugins: [
        // define client config variables
        new webpack.DefinePlugin({
            DEBUG_ENABLED:      JSON.stringify(process.env.DEBUG_ENABLED || false)
        }),

        // clean our build folder
        // new CleanPlugin("dist"),

        // export vendor modules to vendor bundle
        new webpack.optimize.CommonsChunkPlugin({
            name:      "vendor",
            filename:  "js/vendor.bundle.js",
            minChunks: Infinity
        }),

        new BundleAnalyzerPlugin({
            analyzerMode:   "static",
            reportFilename: "webpack-bundle-report.html",
            openAnalyzer:   false
        }),

        function()
        {
            this.plugin("watch-run", function(watching, callback)
            {
                console.log("\n********* Begin compile at " + new Date() + " *********\n");
                callback();
            })
        }
    ],

    module: {
        rules: [
            // typescript
            {
                test: /\.ts$/,
                use:  ["ts-loader"]
            },

            // css
            {
                test: /\.css$/,
                use:  ["style-loader", "css-loader"]
            },

            // sass
            {
                test: /\.scss$/,
                use:  [
                    "style-loader",
                    {loader: "css-loader", options: "sourceMap"},
                    {loader: "sass-loader", options: "sourceMap"}
                ]
            },

            // fonts
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                use:  [
                    {loader: "file-loader", options: "name=fonts/[name].[ext]"}
                ]
            },

            // images
            {
                test: /\.(jpe?g|png|gif)$/i,
                use:  [
                    {loader: "url-loader", options: {limit: 32000, name: "img/[name].[ext]"}}
                ]
            },

            // CSV
            {
                test: /\.csv$/,
                use:  ["raw-loader"]
            },

            // expose phaser components to global scope
            {test: /pixi\.js$/, use: [{loader: "expose-loader", options: "PIXI"}]},
            // {test: /pixi-particles\.js$/, use: [{loader: "expose-loader", options: "pixi-particles"}]},
            {test: /Stats\.js$/, use: ["script-loader"]}
        ]

}
};
