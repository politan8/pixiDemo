/* tslint:disable:no-var-requires */
import Container = PIXI.Container;
import {IPage} from "./IPage";
import ComponentsFactoy from "./ComponentsFactory";
import TextStyle = PIXI.TextStyle;
import Sprite = PIXI.Sprite;
import Text = PIXI.Text;
let emoji:any = require("node-emoji");

export default class TaskTwo extends Container implements IPage
{
    private static UPDATE_INTERVAL:number = 2000;

    private phrases:Array<string> = [
        "I :heart: :coffee:!",
        "meaning :dollar: meaning",
        ":smile: Flexibility :relaxed:",
        "Emoji (:jp: singular emoji, plural emoji or emojis[:four_leaf_clover:])",
        "The :world_map:’s largest HTML5 :video_game: platform",
        "Questions? Let :busts_in_silhouette::arrow_left: :information_desk_person:"
    ];

    private componnetsFactory:ComponentsFactoy;
    private textHolder:Container;
    private textUpdateIntervalId:number;

    public constructor(componnetsFactory:ComponentsFactoy)
    {
        super();
        this.componnetsFactory = componnetsFactory;

        // console.log(emoji.unemojify(''));
    }

    public activatePage():void
    {
        this.visible = true;
        this.createElements();
    }

    public deactivatePage():void
    {
        this.visible = false;
        this.clearElements();
    }

    private createElements():void
    {
        this.textHolder = this.componnetsFactory.createContainer(this);
        this.textUpdateIntervalId = setInterval(() => this.generateRandomText(), TaskTwo.UPDATE_INTERVAL);
        this.generateRandomText();
    }

    private clearElements():void
    {
        if (this.textHolder)
        {
            this.clearText();
            this.textHolder.destroy(true);
            this.textHolder = null;
        }
        clearInterval(this.textUpdateIntervalId);
    }

    private generateRandomText():void
    {
        let text:string = this.phrases[this.getRandomInt(0, this.phrases.length - 1)];
        let style:TextStyle = this.componnetsFactory.getDefaultTextStyle();
        style.fill = "#" + (Math.random() * 0xFFFFFF << 0).toString(16);
        style.fontSize = this.getRandomInt(26, 100);
        style.wordWrap = true;
        style.wordWrapWidth = 600;
        this.writeText(text, style);
    };

    private writeText(content:string, style:TextStyle):void
    {
        this.clearText();

        content = emoji.emojify(content);
        let text:Text = this.componnetsFactory.createText(this.textHolder, content, style);

        this.textHolder.x = (this.componnetsFactory.appSize.width - this.textHolder.width) / 2;
        this.textHolder.y = (this.componnetsFactory.appSize.height - this.textHolder.height) / 2;
    }

    private clearText():void
    {
        this.textHolder.removeChildren();
    }

    private getRandomInt(min:number, max:number):number
    {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}