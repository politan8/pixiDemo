
import Container = PIXI.Container;
import {IPage} from "./IPage";
import ComponentsFactoy from "./ComponentsFactory";
import Texture = PIXI.Texture;
import Sprite = PIXI.Sprite;

/// <reference path="/node_modules/pixi-particles/ambient.d.ts" />
import Emitter = PIXI.particles.Emitter;

export default class TaskThree extends Container implements IPage
{
    private componnetsFactory:ComponentsFactoy;
    private emitterHolder:Container;
    private background:Sprite;
    private emitter:Emitter;
    private timeElapsed:number;

    public constructor(componnetsFactory:ComponentsFactoy)
    {
        super();
        this.componnetsFactory = componnetsFactory;
    }

    public activatePage():void
    {
        this.visible = true;
        this.createElements();
    }

    public deactivatePage():void
    {
        this.visible = false;
        this.clearElements();
        this.componnetsFactory.clearRendererPluginSprites();
    }

    private createElements():void
    {
        this.background = this.componnetsFactory.createImage("head", this);
        let scale:number = 2;
        this.background.scale.set(scale, scale);
        this.componnetsFactory.moveToCanvasCenter(this.background);
        this.background.x -= this.background.width / 2;
        this.background.y -= this.background.height / 2 + 70;

        let images:Array<Texture> = [
            this.componnetsFactory.getTextureFromLoadedImage("fire1"),
            this.componnetsFactory.getTextureFromLoadedImage("fire2"),
            this.componnetsFactory.getTextureFromLoadedImage("fire3")
        ];
        this.emitterHolder = this.componnetsFactory.createContainer(this);
        this.emitter = new Emitter(this.emitterHolder, images,
            {
                "alpha": {
                    "start": 0.79,
                    "end": 0.48
                },
                "scale": {
                    "start": 0.69,
                    "end": 0.35,
                    "minimumScaleMultiplier": 1
                },
                "color": {
                    "start": "#fff191",
                    "end": "#ff622c"
                },
                "speed": {
                    "start": 100,
                    "end": 100,
                    "minimumSpeedMultiplier": 0.001
                },
                "acceleration": {
                    "x": 10,
                    "y": 0
                },
                "maxSpeed": 0,
                "startRotation": {
                    "min": 275,
                    "max": 275
                },
                "noRotation": false,
                "rotationSpeed": {
                    "min": 10,
                    "max": 50
                },
                "lifetime": {
                    "min": 0.1,
                    "max": 0.75
                },
                "blendMode": "normal",
                "frequency": 0.2,
                "emitterLifetime": -1,
                "maxParticles": 10,
                "pos": {
                    "x": 1,
                    "y": 1
                },
                "addAtBack": true,
                "spawnType": "point"
            });

        this.componnetsFactory.moveToCanvasCenter(this.emitterHolder);
        this.componnetsFactory.oefCallback = () => this.update();
        this.timeElapsed = Date.now();
        this.emitter.emit = true;
    }

    private clearElements():void
    {
        this.componnetsFactory.oefCallback = null;
        if (this.emitter)
        {
            this.emitter.emit = false;
            this.emitterHolder.destroy(true);
            this.emitter = null;
        }
        this.background && this.background.destroy();
        this.background = null;
    }

    private update():void
    {
        let timeNow:number = Date.now();

        this.emitter.update((timeNow - this.timeElapsed) * 0.001);
        this.timeElapsed = timeNow;
    }
}