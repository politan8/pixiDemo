import Container = PIXI.Container;
import {IPage} from "./IPage";
import ComponentsFactoy from "./ComponentsFactory";
import Text = PIXI.Text;

export default class NavigationController
{
    private container:Container;
    private menuContainer:Container;
    private btnBack:Container;
    private componentsFactory:ComponentsFactoy;
    private pages:Array<IPage> = [];
    private currentPage:IPage;

    public constructor(container:Container, componentsFactory:ComponentsFactoy)
    {
        this.container = container;
        this.componentsFactory = componentsFactory;

        this.menuContainer = this.componentsFactory.createContainer(this.container);
        this.menuContainer.x = 100;
        this.menuContainer.y = 50;
        this.addBackButton();
    }

    public addPage(name:string, page:IPage):void
    {
        this.pages.push(page);
        let btn:Container = this.componentsFactory.createContainer(this.menuContainer);
        let label:Text = this.componentsFactory.createText(btn, name);
        btn.interactive = true;
        btn.on("pointerup", () => this.activatePage(page));
        btn.y = btn.height + 80 * this.menuContainer.children.length;

        page.deactivatePage();
        this.container.addChild(page);
    }

    private addBackButton():void
    {
        this.btnBack = this.componentsFactory.createContainer(this.container);
        let label:Text = this.componentsFactory.createText(this.btnBack, "Back");
        this.btnBack.interactive = true;
        this.btnBack.on("pointerup", () => this.deactivateCurrentPage());
        this.btnBack.x = this.componentsFactory.appSize.width - this.btnBack.width;
        this.btnBack.visible = false;
    }

    private activatePage(page:IPage):void
    {
        this.deactivateCurrentPage();
        this.currentPage = page;
        this.currentPage.activatePage();
        this.menuContainer.visible = false;
        this.btnBack.visible = true;
        this.forceFullScreen();
    }

    private deactivateCurrentPage():void
    {
        this.currentPage && this.currentPage.deactivatePage();
        this.currentPage = null;
        this.menuContainer.visible = true;
        this.btnBack.visible = false;
    }

    private forceFullScreen():void
    {
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        } else if (document.documentElement["mozRequestFullScreen"]) {
            document.documentElement["mozRequestFullScreen"]();
        } else if (document.documentElement.webkitRequestFullscreen) {
            document.documentElement.webkitRequestFullscreen();
        } else if (document.documentElement["msRequestFullscreen"]) {
            document.documentElement["msRequestFullscreen"]();
        }
    }

}