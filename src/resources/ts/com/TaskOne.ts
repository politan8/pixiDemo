import Container = PIXI.Container;
import {IPage} from "./IPage";
import ComponentsFactoy from "./ComponentsFactory";
import Sprite = PIXI.Sprite;
import Point = PIXI.Point;

export default class TaskOne extends Container implements IPage
{
    private static NUMBER_OF_ITEMS:number = 144;
    private static ITEM_WIDTH:number = 120;
    private static ANIMATION_DURATION:number = 2;
    private static ANIMATION_INTERVAL:number = 1;

    private componnetsFactory:ComponentsFactoy;
    private deck1:Container;
    private deck2:Container;
    private animationContainer:Container;
    private elements:Array<Sprite> = [];
    private timeline:TimelineMax;
    private deckWidth:number;
    private elementDX:number;

    public constructor(componnetsFactory:ComponentsFactoy)
    {
        super();
        this.componnetsFactory = componnetsFactory;
        this.deckWidth = this.componnetsFactory.appSize.width - 40;
        this.elementDX = (this.deckWidth - TaskOne.ITEM_WIDTH) / TaskOne.NUMBER_OF_ITEMS;
    }

    public activatePage():void
    {
        this.visible = true;
        this.createElements();
        this.startAnimation();
    }

    public deactivatePage():void
    {
        this.visible = false;
        this.stopAnimation();
        this.clearElements();
    }

    private createElements():void
    {
        this.deck1 = this.componnetsFactory.createContainer(this);
        this.deck2 = this.componnetsFactory.createContainer(this);
        this.animationContainer = this.componnetsFactory.createContainer(this);
        this.populateDeck(this.deck1);

        this.deck1.x = (this.componnetsFactory.appSize.width - this.deck1.width) / 2;
        this.deck1.y = (this.componnetsFactory.appSize.height / 2 - this.deck1.height) / 2;

        this.deck2.x = this.deck1.x;
        this.deck2.y = this.componnetsFactory.appSize.height / 2 + this.deck2.y;

        this.animationContainer.x = this.deck2.x;
        this.animationContainer.y = this.deck2.y;
    }

    private clearElements():void
    {
        let element:Sprite;
        while (this.elements.length > 0)
        {
            element = this.elements.pop();
            TweenMax.killTweensOf(element);
            element.destroy();
        }
        this.deck1 && this.deck1.destroy();
        this.deck2 && this.deck2.destroy();
        this.deck1 = null;
        this.deck2 = null;
    }

    private startAnimation():void
    {
        this.timeline = new TimelineMax();
        let delay:number = 0;
        this.elements.map((element:Sprite) => {
            this.timeline.addCallback(this.animateElement, delay, [element], this);
            delay += TaskOne.ANIMATION_INTERVAL;
        });

    }

    private animateElement(element:Sprite):void
    {
        let destination:Point = this.getTargetPositionForElement(element);
        this.translateElementToAnimationContainer(element);
        TweenMax.to(element, TaskOne.ANIMATION_DURATION, {x:destination.x, ease:Expo.easeOut});
        TweenMax.to(element, TaskOne.ANIMATION_DURATION, {y:destination.y, ease:Cubic.easeOut, onComplete:() => this.translateElementToTargetContainer(element)});
    }

    private stopAnimation():void
    {
        this.timeline && this.timeline.kill();
        this.timeline = null;
    }

    private populateDeck(container:Container):void
    {
        let image:Sprite = this.componnetsFactory.createImage("card", container);
        let scale:number = TaskOne.ITEM_WIDTH / image.width;
        image.scale.set(scale, scale);
        this.elements.push(image);

        for (let i:number = 1; i < TaskOne.NUMBER_OF_ITEMS; i++)
        {
            image = this.componnetsFactory.createImage("card", container);
            image.scale.set(scale, scale);
            image.x = this.elementDX * i;
            this.elements.unshift(image);
        }
    }

    private getTargetPositionForElement(element:Sprite):Point
    {
        let dest:Container = this.deck2;
        return new Point((dest.children.length + this.animationContainer.children.length) * this.elementDX, 0);
    }

    private translateElementToAnimationContainer(element:Sprite):void
    {
        let src:Container = element.parent;
        let dest:Container = this.animationContainer;
        let newPosition:Point = element.getGlobalPosition();
        newPosition = dest.toLocal(newPosition);
        element.x = newPosition.x;
        element.y = newPosition.y;
        this.setCacheIfNeeded(src, false);
        dest.addChild(element);
        this.setCacheIfNeeded(src, true);
    }

    private translateElementToTargetContainer(element:Sprite):void
    {
        let src:Container = element.parent;
        let dest:Container = this.deck2;
        this.setCacheIfNeeded(dest, false);
        dest.addChild(element);
        this.setCacheIfNeeded(dest, true);
    }

    private setCacheIfNeeded(container:Container, cached:boolean):void
    {
        if (ComponentsFactoy.PIXEL_RATIO < 2)
        {
            container.cacheAsBitmap = cached;
        }
    }
}