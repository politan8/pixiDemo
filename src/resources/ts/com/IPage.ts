import Container = PIXI.Container;
export interface IPage extends Container
{
    activatePage():void;
    deactivatePage():void;
}