import Application = PIXI.Application;
import Sprite = PIXI.Sprite;
import AnimatedSprite = PIXI.extras.AnimatedSprite;
import Texture = PIXI.Texture;
import Container = PIXI.Container;
import Graphics = PIXI.Graphics;
import TextStyleOptions = PIXI.TextStyleOptions;
import Text = PIXI.Text;
import TextMetrics = PIXI.TextMetrics;
import TextStyle = PIXI.TextStyle;
import Rectangle = PIXI.Rectangle;
import Stage = PIXI.core.Stage;
import {IPage} from "./IPage";
import TaskOne from "./TaskOne";
import TaskThree from "./TaskThree";
import TaskTwo from "./TaskTwo";
import DisplayObject = PIXI.DisplayObject;

export default class ComponentsFactoy
{
    public static PIXEL_RATIO:number = 2;
    public static WIDTH:number = 800;
    public static HEIGHT:number = 600;

    private canvasHolder:HTMLDivElement;
    private app:Application;
    private stats:Stats;

    public oefCallback:Function;

    public constructor(showStats?:boolean)
    {
        ComponentsFactoy.PIXEL_RATIO = this.isMobile() ? window.devicePixelRatio : ComponentsFactoy.PIXEL_RATIO;
        this.createPixi();
        showStats && this.createStats();
    }

    public loadAssets(assetsList:Array<{name:string, path:string}>, completeHandler:Function):void
    {
        let assetsPath:string = ComponentsFactoy.PIXEL_RATIO >= 2 ? "/assets/@2x/" : "/assets/";
        assetsList.map((asset) => PIXI.loader.add(asset.name, assetsPath + asset.path));
        PIXI.loader.load((loader, resources) =>
            {
                completeHandler && completeHandler();
            });
    }

    public createContainer(parent?:Container, bringToBack?:boolean):Container
    {
        let container:Container = new Container();
        if (parent)
        {
            bringToBack ? parent.addChildAt(container, 0) : parent.addChild(container);
        }
        return container;
    }

    public createImage(assetName:string, parent?:Container):Sprite
    {
        // let image:Sprite = new Sprite(PIXI.Texture.fromFrame(assetName));
        let image:Sprite = new Sprite(this.getTextureFromLoadedImage(assetName));
        if (parent)
        {
            parent.addChild(image);
        }
        return image;
    }

    public getTextureFromLoadedImage(assetName:string):Texture
    {
        let texture:Texture = PIXI.loader.resources[assetName].texture;
        return texture;
    }

    public createText(parent:Container, mess:string = "", style:TextStyleOptions = null, x:number = 0, y:number = 0):Text
    {
        style = style || this.getDefaultTextStyle();
        let text:Text = new Text(mess, style);
        text.x = x;
        text.y = y;
        if (parent)
        {
            parent.addChild(text);
        }
        return text;
    }

    public getDefaultTextStyle():TextStyle
    {
        let style:TextStyle = new PIXI.TextStyle({
            fontFamily: "Arial",
            fontSize: 28,
            // fontStyle: 'italic',
            fontWeight: "bold",
            fill: "#ffffff",
            // stroke: '#4a1850',
            // strokeThickness: 5,
            // dropShadow: true,
            // dropShadowColor: "#000000",
            // dropShadowBlur: 1,
            // dropShadowAngle: Math.PI / 6,
            // dropShadowDistance: 6,
            // wordWrap: true,
            // wordWrapWidth: 440
        });
        return style;
    }

    public createTaskOne():IPage
    {
        let page:IPage = new TaskOne(this);
        return page;
    }

    public createTaskTwo():IPage
    {
        let page:IPage = new TaskTwo(this);
        return page;
    }

    public createTaskThree():IPage
    {
        let page:IPage = new TaskThree(this);
        return page;
    }

    public get appSize():Rectangle
    {
        let width:number = this.adjustPixelRatio(this.app.renderer.width);
        let height:number = this.adjustPixelRatio(this.app.renderer.height);
        // let width:number = ComponentsFactoy.adjustPixelRatio(this.app.view.width);
        // let height:number = ComponentsFactoy.adjustPixelRatio(this.app.view.height);
        return new Rectangle(0, 0, width, height);
    }

    public get stage():Stage
    {
        return this.app.stage;
    }

    public clearRendererPluginSprites():void
    {
        this.app.renderer.plugins.sprite.sprites.length = 0;
    }

    public moveToCanvasCenter(element:DisplayObject):void
    {
        //TODO: calculate absolute position regardless of parent location
        element.x = this.appSize.width / 2;
        element.y = this.appSize.height / 2;
    }

    private createPixi():void
    {
        let canvas:HTMLCanvasElement = <HTMLCanvasElement> document.createElement("canvas");
        this.canvasHolder = <HTMLDivElement> document.body.getElementsByClassName("client")[0];
        this.canvasHolder.appendChild(canvas);
        this.app = new Application({view: canvas, width: ComponentsFactoy.WIDTH, height: ComponentsFactoy.HEIGHT, transparent: true, resolution:ComponentsFactoy.PIXEL_RATIO});
        this.app.ticker.add(() => this.oef());
        this.resizeApp();
        window.addEventListener("resize", () => this.resizeApp());
    }

    private resizeApp():void
    {
        let scaleFactor:number = Math.min(this.canvasHolder.clientWidth / ComponentsFactoy.WIDTH, this.canvasHolder.clientHeight / ComponentsFactoy.HEIGHT);
        let targetWidth:number = ComponentsFactoy.WIDTH * scaleFactor;
        let targetHeight:number = ComponentsFactoy.HEIGHT * scaleFactor;
        this.app.renderer.view.style.left = (this.canvasHolder.clientWidth - targetWidth) / 2 + "px";
        this.app.renderer.view.style.width = ~~(targetWidth) + "px";
        this.app.renderer.view.style.height = ~~(targetHeight) + "px";
    }

    private oef():void
    {
        if (this.stats)
        {
            this.stats.update();
        }
        this.oefCallback && this.oefCallback();
    }

    private createStats():void
    {
        this.stats = new Stats();
        this.stats.showPanel(0);
        document.body.appendChild(this.stats.dom);
    }

    private isMobile():boolean
    {
        return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4)));
    }

    private adjustPixelRatio(value:number):number
    {
        return value / ComponentsFactoy.PIXEL_RATIO;
    }

}