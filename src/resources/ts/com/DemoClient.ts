/* tslint:disable:no-var-requires */

import ComponentsFactoy from "./ComponentsFactory";
import Container = PIXI.Container;
import NavigationController from "./NavigationController";

export default class DemoClient
{
    private componnetsFactory:ComponentsFactoy;
    private menu:NavigationController;

    public launch():void
    {
        this.initComponnetsFactory();
    }

    private initComponnetsFactory():void
    {
        this.componnetsFactory = new ComponentsFactoy(true);

        let assetsList:Array<{name:string, path:string}> = [];
        assetsList.push({name:"card", path:"credit-card-back.png"});
        assetsList.push({name:"fire1", path:"fire1.png"});
        assetsList.push({name:"fire2", path:"fire2.png"});
        assetsList.push({name:"fire3", path:"fire3.png"});
        assetsList.push({name:"head", path:"head1.jpeg"});
        this.componnetsFactory.loadAssets(assetsList, () => this.assetsLoadCompleteHandler());
    }

    private initNavigationMenu():void
    {
        let layer:Container = this.componnetsFactory.createContainer(this.componnetsFactory.stage);
        this.menu = new NavigationController(layer, this.componnetsFactory);
        this.menu.addPage("Everyday I'm Shuffling " + ComponentsFactoy.PIXEL_RATIO, this.componnetsFactory.createTaskOne());
        this.menu.addPage("Emoticon", this.componnetsFactory.createTaskTwo());
        this.menu.addPage("Set on Fire", this.componnetsFactory.createTaskThree());
    }

    private assetsLoadCompleteHandler():void
    {
        this.initNavigationMenu();
    }
}